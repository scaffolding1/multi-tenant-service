package com.test.rest.config;

import lombok.extern.slf4j.Slf4j;
import org.keycloak.adapters.KeycloakConfigResolver;
import org.keycloak.adapters.KeycloakDeployment;
import org.keycloak.adapters.KeycloakDeploymentBuilder;
import org.keycloak.adapters.OIDCHttpFacade;
import org.keycloak.representations.adapters.config.AdapterConfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class HeaderBasedConfigResolver implements KeycloakConfigResolver {

    private final ConcurrentHashMap<String, KeycloakDeployment> cache = new ConcurrentHashMap<>();

    private static AdapterConfig adapterConfig;

    @Override
    public KeycloakDeployment resolve(OIDCHttpFacade.Request request) {
        String realm = request.getHeader("realm");
        if(!cache.containsKey(realm)) {
            //InputStream is = getClass().getResourceAsStream("/"+realm+"-keycloak.json");
            InputStream is;
            try {
                is = getFileAsStream(realm);
                cache.put(realm, KeycloakDeploymentBuilder.build(is));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                log.error("Error while processing tenant config");
            }
        }
        return cache.get(realm);
    }

    static void setAdapterConfig(AdapterConfig adapterConfig) {
        HeaderBasedConfigResolver.adapterConfig = adapterConfig;
    }

    private InputStream getFileAsStream(String realm) throws FileNotFoundException {
        String path = System.getProperty("tenant.config.path");
        File file = new File(path + "/" + realm + "-keycloak.json");
        return new FileInputStream(file);
    }
}
