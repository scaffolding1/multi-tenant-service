# Multi-Tenant-Service

## Introduction
This is a sample REST microservice which demonstrate multi-tenancy service with keycloak server.
The basic setup (single tenancy) of the microservice is explained [here](https://gitlab.com/scaffolding1/restservice) along with
keycloak configuration

**_Note:_**
> There few differences in the configuration from single tenancy application which are described in the following section

## Configurations
Unless single tenancy, the configurations in `application.properties` are generic.
Below are the application configurations need to be set for keycloak under `application.properties`
 - spring.autoconfigure.exclude = org.keycloak.adapters.springboot.KeycloakAutoConfiguration (No need to change this)
 - security.ignored = /unsecured (change the URI if you have non-secured endpoints)
 - keycloak.security-constraints[0].authRoles[0]=user 
 - keycloak.security-constraints[0].securityCollections[0].patterns[0]=/user
 - keycloak.security-constraints[1].authRoles[0]=admin
 - keycloak.security-constraints[1].securityCollections[0].patterns[0]=/admin

The parameters prefixed with `keycloak.security-constraints` are according to the client setup for the realm in keycloak
and the URI of the REST application. 

> Please note the `server.port` is set to `8888` because the keycloak server will be running on port `8080`
> Please feel free to change it to your favourite port number!

**Important**
The Realm related configurations for every tenant are placed under `resources` directory need to be externalized.
For this reason set the environment property `tenant.config.path` during the runtime.  

For e.g., run the service as below:
`java -Dtenant.config.path="D:/config" -jar MultiTenancyService-1.0-SNAPSHOT.jar` and place the realm configuration 
under the configured directory. Application will read the json files during the runtime.

## How to Test
Testing instructions are described [here](https://gitlab.com/scaffolding1/restservice#how-to-test)
